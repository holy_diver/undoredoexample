﻿using System;
using Events.Event;
using MainApplication.Models;

namespace MainApplication.Events
{
    public class PayDebtEvent : StateEvent
    {
        public decimal Debt { get; }
        public override string EventInformation { get; }
        public EventDrivenModel Model { get; }
        protected override Action DoAction { get; }
        protected override Action UndoAction { get; }
        public UserModel UserModel { get; }

        public PayDebtEvent(UserModel model, decimal debt)
        {
            Debt = debt;
            Model = model;
            UserModel = model;

            DoAction = () => { model.Balance -= debt; };
            UndoAction = () => { model.Balance += debt; };
            
            EventInformation = "Debt of " + debt;
        }
    }
}