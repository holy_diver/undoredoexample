﻿using System;
using Events.Event;
using MainApplication.Models;

namespace MainApplication.Events
{
    public class ApplyInterestEvent : StateEvent
    {
        public decimal InterestRatio { get; }
        public override string EventInformation { get; }
        protected override Action DoAction { get; }
        protected override Action UndoAction { get; }
        public UserModel UserModel { get; }

        public ApplyInterestEvent(UserModel model, decimal interestRatio)
        {
            InterestRatio = interestRatio;
            UserModel = model;

            DoAction = () => { model.Balance *= interestRatio; };
            UndoAction = () => { model.Balance /= interestRatio; };

            EventInformation = "Interest of " + interestRatio;
        }
    }
}